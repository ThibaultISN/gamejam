import pygame


class HomeView:
    def __init__(self, fenetre,img):
        self.fenetre = fenetre
        self.fond = pygame.image.load(img).convert()
        self.fenetre.blit(self.fond, (0,0))
        self.currentTime = 0

    def rafraichir(self):
        self.fenetre.blit(self.fond, (0,0))
        if self.currentTime != 0:
            self.fenetre.blit(self.currentTime, (425,230))
        pygame.display.flip()

    def getFenetre(self):
        return self.fenetre

    def setFond(self, img):
        self.fond = pygame.image.load(img).convert()

    def setTemps(self, currentTime):
        self.currentTime = currentTime
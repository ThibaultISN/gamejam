import pygame
from pygame import *


class GameView:
    def __init__(self, fenetre):
        self.fenetre = fenetre
        self.fond = pygame.image.load("asset/fondJeu.png").convert()
        self.fenetre.blit(self.fond, (0, 0))

    def rafraichir(self, player, plateforms, wirlwinds, fires, objects, inactiveObject, currentTime, currentScene):
        self.fenetre.blit(self.fond, (0, 0))
        self.rafraichirObjects(inactiveObject)
        self.rafraichirObjects(fires)
        self.rafraichirObjects(plateforms)
        self.rafraichirObjects(objects)
        self.fenetre.blit(player.image, player.rect)
        self.rafraichirObjects(wirlwinds)
        self.fenetre.blit(currentTime, (10, 10))
        self.fenetre.blit(currentScene, (890, 10))
        pygame.display.flip()


    def rafraichirObjects(self, objects):
        for object in objects:
            self.fenetre.blit(object.image, object.rect)

    def setFond(self, img):
        self.fond = pygame.image.load(img).convert()
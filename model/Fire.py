from model import AnimateObject


class Fire(AnimateObject.AnimateObject):
    def __init__(self, x, y, taille=1):
        imgs = []
        if taille == 1:
            imgs.append("asset/fire-1.png")
            imgs.append("asset/fire-2.png")
            imgs.append("asset/fire-3.png")
        if taille == 2:
            imgs.append("asset/fire1024-1.png")
            imgs.append("asset/fire1024-2.png")
            imgs.append("asset/fire1024-3.png")

        AnimateObject.AnimateObject.__init__(self, imgs, x, y+10)

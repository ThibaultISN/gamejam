from model import ImgObject


class Object(ImgObject.ImgObject):
    def __init__(self, img, x, y, ox=False, oy=False, tag = "standardObject"):
        ImgObject.ImgObject.__init__(self, img, x, y, ox, oy)
        self.tag = tag
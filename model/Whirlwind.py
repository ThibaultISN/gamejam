from model import AnimateObject


class Whirlwind(AnimateObject.AnimateObject):
    puissance: int  # puissance de wirlwind
    orientation: bool  # si le wirlwind est vertical

    def __init__(self, puissance, orientation, x, y, taille=1):
        imgs = []
        if taille == 1:
            imgs.append("asset/wirlwind150-1.png")
            imgs.append("asset/wirlwind150-2.png")
            imgs.append("asset/wirlwind150-3.png")
        if taille == 2:
            imgs.append("asset/wirlwind250-1.png")
            imgs.append("asset/wirlwind250-2.png")
            imgs.append("asset/wirlwind250-3.png")
        ox = puissance > 0
        AnimateObject.AnimateObject.__init__(self, imgs, x, y, ox, not orientation)
        self.puissance = puissance
        self.orientation = orientation

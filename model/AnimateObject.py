import pygame

from model import Object


class AnimateObject(Object.Object):
    imgs: []
    images: []
    def __init__(self, imgs, x, y, ox=False, oy=False, tag = "standardObject"):
        self.index = 0
        self.images = []
        self.images.append(pygame.image.load(imgs[0]))
        self.images.append(pygame.image.load(imgs[1]))
        self.images.append(pygame.image.load(imgs[2]))
        self.ox = ox
        self.oy = oy
        Object.Object.__init__(self, imgs[0], x, y, self.ox, self.oy, tag)

    def animation(self):
        self.index += 1
        if self.index >= 30:
            self.index = 0
        self.image = self.images[self.index // 10]
        self.image = pygame.transform.flip(self.image, 0, self.ox)
        if self.oy:
            self.image = pygame.transform.rotate(self.image, 90)

    def update(self, *args):
        self.animation()
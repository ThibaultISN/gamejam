import pygame

from model import ImgObject, Environement


class Player(ImgObject.ImgObject):
    accelerationY = 0  # acceleration verticale
    accelerationX = 0  # acceleration horizontale

    isGrounded = False  # etat "est au sol"
    haveLeaf = False
    useLeaf = False
    onWirlwind = False
    orientationLeft = False
    isWalk = False

    imagesWalk: []
    imagesGlide: []

    def __init__(self, x, y, haveLeaf=True):
        ImgObject.ImgObject.__init__(self, "asset/koala-right.png", x, y)
        self.index = 0
        self.haveLeaf = haveLeaf
        self.imagesWalk = []
        self.imagesGlide = []

        self.initImages()

        # ajustement de l'image
        self.image = pygame.transform.smoothscale(self.image, (75, 75))  # redimentionne de manière smooooooth
        self.rect = self.image.get_rect()  # on redefini le rect
        self.rect = self.rect.move(x, y)  # on deplace le rect au bonne coordonées

    def initImages(self):
        self.imageJump = pygame.transform.smoothscale(pygame.image.load("asset/koala-right-jump.png"), (75, 75))
        self.imagesWalk.append(pygame.transform.smoothscale(pygame.image.load("asset/koala-right.png"), (75, 75)))
        self.imagesWalk.append(
            pygame.transform.smoothscale(pygame.image.load("asset/koala-right-walk-1.png"), (75, 75)))
        self.imagesWalk.append(pygame.transform.smoothscale(pygame.image.load("asset/koala-right.png"), (75, 75)))
        self.imagesWalk.append(
            pygame.transform.smoothscale(pygame.image.load("asset/koala-right-walk-2.png"), (75, 75)))
        self.imagesGlide.append(
            pygame.transform.smoothscale(pygame.image.load("asset/koala-right-glide-1.png"), (75, 100)))
        self.imagesGlide.append(
            pygame.transform.smoothscale(pygame.image.load("asset/koala-right-glide-2.png"), (75, 100)))

    def animation(self):
        self.index += 1
        if self.onWirlwind and self.useLeaf:
            self.animationWirlwind()
        elif self.isGrounded and self.isWalk:
            self.animationWalk()
        elif self.isGrounded:
            self.animationDefault()
        elif not self.isGrounded and self.useLeaf:
            self.animationGlide()
        elif not self.isGrounded:
            self.animationJump()

    def animationJump(self):
        self.image = self.imageJump
        if self.orientationLeft:
            self.image = pygame.transform.flip(self.image, True, False)

    def animationGlide(self):
        self.image = self.imagesGlide[0]
        if self.orientationLeft:
            self.image = pygame.transform.flip(self.image, True, False)

    def animationDefault(self):
        self.image = self.imagesWalk[0]
        if self.orientationLeft:
            self.image = pygame.transform.flip(self.image, True, False)

    def animationWirlwind(self):
        if self.index >= 10:
            self.index = 0
        self.image = self.imagesGlide[self.index // 5]
        if self.orientationLeft:
            self.image = pygame.transform.flip(self.image, True, False)

    def animationWalk(self):
        if self.index >= 40:
            self.index = 0
        self.image = self.imagesWalk[self.index // 10]
        if self.orientationLeft:
            self.image = pygame.transform.flip(self.image, True, False)

    def deplacement(self):
        if pygame.key.get_pressed()[pygame.K_LEFT]:  # si la flêche de gauche est pressé
            self.accelerationX = -5
            self.orientationLeft = True
            if self.isGrounded:
                self.isWalk = True
        if pygame.key.get_pressed()[pygame.K_RIGHT]:  # si la flêche de droite est pressé
            self.accelerationX = 5
            self.orientationLeft = False
            if self.isGrounded:
                self.isWalk = True
        if pygame.key.get_pressed()[pygame.K_DOWN] and self.isGrounded:
            self.rect = self.rect.move(0, 1)
        if pygame.key.get_pressed()[pygame.K_UP] and self.isGrounded:  # si espace est pressé
            self.accelerationY = -5

    def update(self, *args):
        self.updateWalk()
        self.updateUseLeaf()
        self.velocityUpdate(args[0])
        self.animation()

    def updateWalk(self):
        if not self.isGrounded or (
                not pygame.key.get_pressed()[pygame.K_LEFT] and not pygame.key.get_pressed()[pygame.K_RIGHT]):
            self.isWalk = False

    def updateUseLeaf(self):
        if self.haveLeaf and pygame.key.get_pressed()[pygame.K_SPACE]:
            self.useLeaf = True
        else:
            self.useLeaf = False

    def velocityUpdate(self, environement):
        if not self.isGrounded:  # si le joueur n'est pas au sol
            self.accelerationY += environement.gravityScale  # l'acceleration vertical subit la gravité a hauteur de sa puissance
            self.rect = self.rect.move(self.accelerationX, self.accelerationY)
        else:
            self.rect = self.rect.move(self.accelerationX, self.accelerationY)
            self.accelerationX = 0

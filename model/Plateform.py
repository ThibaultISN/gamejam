import pygame

from model import Object


class Plateform(Object.Object):
    def __init__(self, x, y, taille=1, reverse=False):
        if taille == 1:
            img = "asset/plateform100.png"
        elif taille == 2:
            img = "asset/plateform200.png"

        Object.Object.__init__(self, img, x, y)

        if reverse:
            self.image = pygame.transform.flip(self.image, True, False)
import pygame


class ImgObject(pygame.sprite.Sprite):
    def __init__(self, img, x, y, ox=False, oy=False):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(img).convert_alpha()
        if oy:
            self.image = pygame.transform.rotate(self.image, 180)
        if ox:
            self.image = pygame.transform.rotate(self.image, -90)
        self.rect = self.image.get_rect()
        self.rect = self.rect.move(x, y)
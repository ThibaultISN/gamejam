import pygame

from model.Environement import Environement
from view import GameView
from model import Player, Object, Environement, Whirlwind, Fire, Plateform, AnimateObject


class GameControler:
    gameView: GameView  # vue du jeu
    environement: Environement  # variable de stockage des données de l'environnement
    player: Player  # variable joueur
    plateforms: pygame.sprite.Group  # groupe des objets "plateform"
    whirlwinds: pygame.sprite.Group  # groupe des objets "wirlwind"
    objects: pygame.sprite.Group  # groupe des objets interagissable

    def __init__(self, fenetre, musics):
        self.gameView = GameView.GameView(fenetre)
        self.environement = Environement.Environement(0.25)
        self.plateforms = pygame.sprite.Group()
        self.whirlwinds = pygame.sprite.Group()
        self.objects = pygame.sprite.Group()
        self.fires = pygame.sprite.Group()
        self.inactiveObjects = pygame.sprite.Group()
        self.animateObjects = pygame.sprite.Group()
        self.gameOver = False
        self.victoire = False
        self.scene = 1
        self.creerScene(self.scene)
        self.songInit(musics)
        self.tps0 = pygame.time.get_ticks()

    def songInit(self, musics):
        for music in musics:
            music.play(-1)

    def traiterEvenement(self, event):
        self.player.deplacement()

    def gameUpdate(self):
        self.checkGameOver()
        self.updateTime()
        self.updateScene()
        self.playerOnPlateformCollider()
        self.playerOnWhirlwindCollider()
        self.playerOnObjectCollider()
        self.playerOnFireCollider()
        self.player.update(self.environement)
        self.animateObjects.update()
        self.gameView.rafraichir(self.player, self.plateforms, self.whirlwinds, self.fires, self.objects,
                                 self.inactiveObjects, self.currentTime, self.currentScene)

    def checkGameOver(self):
        if self.player.rect.left == -50 or self.player.rect.left == 1075:
            self.gameOver = True

    def updateTime(self):
        if not self.gameOver:
            font = pygame.font.Font(None, 30)
            self.Time = (pygame.time.get_ticks() - self.tps0) / 1000
            self.currentTime = font.render(self.Time.__str__(), 1, (255, 255, 255))

    def updateScene(self):
        if not self.gameOver:
            font = pygame.font.Font(None, 30)
            self.currentScene = font.render("Niveau : " + self.scene.__str__() + " / 5", 1, (255, 255, 255))

    def playerOnPlateformCollider(self):
        hitbox = self.player.rect.inflate(0, -70)
        hitbox = hitbox.move(0, 35)
        Ground = False
        for plateform in self.plateforms:
            if hitbox.colliderect(plateform.rect.inflate(0, -10)):
                Ground = True
                break

        if Ground:  # si la liste des objets dans "plateforms" en collision avec le joueur n'est pas vide
            self.player.isGrounded = True  # le joueur est sur une plateforme
            if self.player.accelerationY > 0:  # si l'acceleration verticale du joeur est superieur a 0 donc qu'il descend
                self.player.accelerationY = 0  # on met l'acceleration verticale du joueur à 0
        else:
            self.player.isGrounded = False  # le joueur n'est pas sur une plateforme

    def playerOnWhirlwindCollider(self):
        if self.player.useLeaf:
            wirlwindsList = pygame.sprite.spritecollide(self.player, self.whirlwinds, 0)
            for wirlwind in wirlwindsList:  # pour tout les objet wirlwind dans la liste des objects dans "wirlwinds" en colision avec le joueur
                if wirlwind.orientation:  # si le wirlwind est vertical
                    self.player.accelerationY += wirlwind.puissance  # on augmente l'acceleration verticale du joueur avec la puissance du wirlwind
                else:
                    self.player.accelerationX += wirlwind.puissance  # on augmente l'acceleration horizontale du joueur avec la puissance du wirlwind
            if wirlwindsList:
                self.player.onWirlwind = True
            else:
                self.player.onWirlwind = False

    def playerOnObjectCollider(self):
        for object in pygame.sprite.spritecollide(self.player, self.objects, True):
            if object.tag == "leaf":
                pygame.mixer.Sound("asset/recupere_feuille.ogg").play()
                self.player.haveLeaf = True
                self.player.image = pygame.transform.smoothscale(pygame.image.load("asset/koala-right-get-leaf.png"),
                                                                 (75, 100))
                self.gameView.rafraichir(self.player, self.plateforms, self.whirlwinds, self.fires, self.objects,
                                         self.inactiveObjects, self.currentTime, self.currentScene)
                pygame.time.wait(500)
            if object.tag == "door":
                pygame.mixer.Sound("asset/level-up-sound-effects.ogg").play()
                self.scene += 1
                self.creerScene(self.scene)
            if object.tag == "winDoor":
                pygame.mixer.Sound("asset/vin.ogg").play()
                self.victoire = True
            if object.tag == "border":
                self.gameOver = True

    def playerOnFireCollider(self):
        if pygame.sprite.spritecollide(self.player, self.fires, False):
            self.gameOver = True

    def creerScene(self, scene):
        self.objects.empty()
        self.inactiveObjects.empty()
        self.plateforms.empty()
        self.whirlwinds.empty()
        self.fires.empty()
        self.animateObjects.empty()

        if scene == 1:
            self.player = Player.Player(0, 550, False)

            self.objects.add(Object.Object("asset/leaf.png", 875, 25, False, False, "leaf"))
            self.objects.add(Object.Object("asset/door.png", 25, 25, False, False, "door"))

            self.inactiveObjects.add(Object.Object("asset/tronc.png", 100, 0))
            self.inactiveObjects.add(Object.Object("asset/tronc.png", 550, 0))
            self.inactiveObjects.add(Object.Object("asset/tronc.png", 950, 0))

            self.plateforms.add(Plateform.Plateform(0, 125, 1,
                                                    True))  # creation de plateforme et ajout de plateforme dans le Group "plateforms"
            self.plateforms.add(Plateform.Plateform(0, 650, 1, True))
            self.plateforms.add(Plateform.Plateform(175, 175, 2))
            self.plateforms.add(Plateform.Plateform(175, 625, 2))
            self.plateforms.add(Plateform.Plateform(450, 575, 1, True))
            self.plateforms.add(Plateform.Plateform(575, 550, 2))
            self.plateforms.add(Plateform.Plateform(625, 500, 1))
            self.plateforms.add(Plateform.Plateform(800, 125, 2, True))
            self.plateforms.add(Plateform.Plateform(825, 175, 2, True))
            self.plateforms.add(Plateform.Plateform(850, 225, 1, True))
            self.plateforms.add(Plateform.Plateform(850, 275, 2, True))
            self.plateforms.add(Plateform.Plateform(800, 325, 2, True))
            self.plateforms.add(Plateform.Plateform(850, 375, 1, True))
            self.plateforms.add(Plateform.Plateform(875, 425, 1, True))
            self.plateforms.add(Plateform.Plateform(850, 450, 1, True))
            self.plateforms.add(Plateform.Plateform(900, 550, 1, True))

            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 350, 375, 2))

            self.fires.add(Fire.Fire(900, 475))

            self.fires.add(Fire.Fire(0, 718, 2))


        elif scene == 2:
            self.player = Player.Player(25, 425)

            self.objects.add(Object.Object("asset/door.png", 850, 0, False, False, "door"))

            self.inactiveObjects.add(Object.Object("asset/tronc.png", 100, 0))
            self.inactiveObjects.add(Object.Object("asset/tronc.png", 625, 0))
            self.inactiveObjects.add(Object.Object("asset/tronc.png", 950, 0))

            self.plateforms.add(Plateform.Plateform(-25, 500, 2, True))
            self.plateforms.add(Plateform.Plateform(425, 475, 2, True))
            self.plateforms.add(Plateform.Plateform(850, 100, 1, True))

            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 200, 325, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(0.5, True, 300, 325, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-1, True, 725, 425, 2))

            self.fires.add(Fire.Fire(0, 718, 2))

        elif scene == 3:

            self.player = Player.Player(525, 450)

            self.objects.add(Object.Object("asset/door.png", 900, 600, False, False, "door"))

            self.inactiveObjects.add(Object.Object("asset/tronc.png", 50, 0))
            self.inactiveObjects.add(Object.Object("asset/tronc.png", 250, 0))
            self.inactiveObjects.add(Object.Object("asset/tronc.png", 675, 0))
            self.inactiveObjects.add(Object.Object("asset/tronc.png", 1000, 0))

            self.plateforms.add(Plateform.Plateform(150, 100, 1, True))
            self.plateforms.add(Plateform.Plateform(75, 550, 1))
            self.plateforms.add(Plateform.Plateform(300, 725, 1))
            self.plateforms.add(Plateform.Plateform(475, 550, 2, True))
            self.plateforms.add(Plateform.Plateform(725, 575, 1))
            self.plateforms.add(Plateform.Plateform(900, 250, 1, True))
            self.plateforms.add(Plateform.Plateform(925, 350, 1, True))
            self.plateforms.add(Plateform.Plateform(925, 475, 1, True))
            self.plateforms.add(Plateform.Plateform(900, 700, 1, True))

            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 200, 500, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 200, 300, 1))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 300, 550, 1))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, False, 425, 0, 1))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, False, 400, 150, 2))

            self.fires.add(Fire.Fire(725, 500, 1))
            self.fires.add(Fire.Fire(300, 650, 1))
            self.fires.add(Fire.Fire(75, 475, 1))
            self.fires.add(Fire.Fire(0, 718, 2))

        elif scene == 4:

            self.player = Player.Player(0, 600)

            self.objects.add(Object.Object("asset/door.png", 900, 500, False, False, "door"))

            self.inactiveObjects.add(Object.Object("asset/tronc.png", 100, 0))
            self.inactiveObjects.add(Object.Object("asset/tronc.png", 475, 0))
            self.inactiveObjects.add(Object.Object("asset/tronc.png", 825, 0))

            self.plateforms.add(Plateform.Plateform(0, 700, 1, True))
            self.plateforms.add(Plateform.Plateform(175, 475, 1))
            self.plateforms.add(Plateform.Plateform(175, 575, 2))
            self.plateforms.add(Plateform.Plateform(175, 625, 1))
            self.plateforms.add(Plateform.Plateform(175, 675, 2))
            self.plateforms.add(Plateform.Plateform(275, 125, 2, True))
            self.plateforms.add(Plateform.Plateform(375, 350, 1, True))
            self.plateforms.add(Plateform.Plateform(375, 525, 1, True))
            self.plateforms.add(Plateform.Plateform(550, 150, 1))
            self.plateforms.add(Plateform.Plateform(750, 200, 1, True))
            self.plateforms.add(Plateform.Plateform(625, 325, 2, True))
            self.plateforms.add(Plateform.Plateform(900, 600, 1))

            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 0, 400, 1))
            self.whirlwinds.add(Whirlwind.Whirlwind(0.5, False, 0, 150, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 275, 300, 2))

            self.fires.add(Fire.Fire(275, 500, 1))
            self.fires.add(Fire.Fire(450, 275, 1))
            self.fires.add(Fire.Fire(725, 125, 1))
            self.fires.add(Fire.Fire(0, 718, 2))

        elif scene == 5:

            self.gameView.setFond("asset/fond-canope.png")

            self.player = Player.Player(0, 600)

            imgs = []
            imgs.append("asset/fleche-1.png")
            imgs.append("asset/fleche-2.png")
            imgs.append("asset/fleche-3.png")
            windoor = AnimateObject.AnimateObject(imgs, 0, 0, False, False, "winDoor")
            self.objects.add(windoor)
            self.animateObjects.add(windoor)

            self.plateforms.add(Plateform.Plateform(0, 700, 2, False))
            self.plateforms.add(Plateform.Plateform(250, 700, 2, True))
            self.plateforms.add(Plateform.Plateform(725, 700, 1, True))
            self.plateforms.add(Plateform.Plateform(475, 675, 2, False))
            self.plateforms.add(Plateform.Plateform(850, 700, 2, False))

            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 0, 450, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 100, 400, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 200, 450, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 300, 400, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 400, 450, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 500, 400, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 600, 450, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 700, 400, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 800, 450, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 900, 400, 2))
            self.whirlwinds.add(Whirlwind.Whirlwind(-0.5, True, 1000, 450, 2))

            self.fires.add(Fire.Fire(0, 718, 2))

        for object in self.whirlwinds:
            self.animateObjects.add(object)
        for object in self.fires:
            self.animateObjects.add(object)
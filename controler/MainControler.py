from model import Button
from view import HomeView
from controler import GameControler
import pygame


class MainControler:
    partieEnCours = False
    whereWeAre = 1
    boutonJouab = Button.Button(73,248)
    boutonCredit = Button.Button(633,248)
    clock = pygame.time.Clock()

    def __init__(self):
        pygame.init()
        self.musics =[]
        self.musics.append(pygame.mixer.Sound("asset/Aerocity-Stranger.ogg"))
        self.musics.append(pygame.mixer.Sound("asset/feu.ogg"))
        self.musics.append(pygame.mixer.Sound("asset/vent.ogg"))
        fenetre = self.initialiserFenetre()
        self.homeView = HomeView.HomeView(fenetre, "asset/fondMenu.png")
        pygame.key.set_repeat(1)
        self.mainLoop()

    def initialiserFenetre(self):
        fenetre = pygame.display.set_mode((1024, 768))
        pygame.display.set_caption("Eucalypse")
        pygame.display.set_icon(pygame.image.load("asset/icon.png"))
        return fenetre

    def mainLoop(self):
        self.end = False
        while not self.end:
            self.clock.tick(60)
            self.traiterEvenement()
            if not self.partieEnCours:
                self.homeView.rafraichir()
            else:
                if not (self.gameControler.gameOver or self.gameControler.victoire):
                    self.gameControler.gameUpdate()
                elif self.gameControler.gameOver:
                    self.homeView.setFond("asset/fond-game-over.png")
                    self.homeView.rafraichir()
                elif self.gameControler.victoire:
                    self.homeView.setFond("asset/fond-victoire.png")
                    font = pygame.font.Font(None, 80)
                    currentTime = font.render(self.gameControler.Time.__str__(), 1, (127, 188, 244))
                    self.homeView.setTemps(currentTime)
                    self.homeView.rafraichir()
        pygame.quit()

    def lancerPartie(self):
        self.gameControler = GameControler.GameControler(self.homeView.getFenetre(), self.musics)
        self.partieEnCours = True

    def traiterEvenement(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.end = True
            if not self.partieEnCours:
                if self.whereWeAre == 1:
                    if pygame.key.get_pressed()[pygame.K_RETURN]:
                            self.lancerPartie()
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        self.traiterClick(event)
                if self.whereWeAre > 1 :
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_RETURN:
                            self.whereWeAre = 1
                            self.homeView.setFond("asset/fondMenu.png")
                            pygame.time.wait(10)
            else:
                if self.gameControler.gameOver or self.gameControler.victoire:
                    if pygame.key.get_pressed()[pygame.K_RETURN]:
                            self.homeView.currentTime = 0
                            self.partieEnCours = False
                            self.homeView.setFond("asset/fondMenu.png")
                            pygame.time.wait(10)
                            pygame.mixer.stop()
                else:
                    self.gameControler.traiterEvenement(event)

    def traiterClick(self, event):
        hitbox = pygame.rect.Rect(pygame.mouse.get_pos(), (1,1))
        if hitbox.colliderect(self.boutonJouab.rect):
            self.whereWeAre = 2
            self.homeView.setFond("asset/fond-jouabilite.png")
        if hitbox.colliderect(self.boutonCredit.rect):
            self.whereWeAre = 3
            self.homeView.setFond("asset/fond-credits.png")